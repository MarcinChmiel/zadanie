class FileImport {
  static readTextFileToStringAsync = async (file) => {
    return new Promise((resolve, reject) => {
      if (file == null) reject("invalid file");
      if (this.fileExtension(file.name) !== "txt") {
        reject(new Error("invalid file type (only .txt files allowed)"));
      }
      const reader = new FileReader();

      reader.onload = () => {
        var text = reader.result;
        resolve(text);
      };
      reader.readAsText(file);
    });
  };

  static fileExtension = (fileName) => {
    return fileName.substring(fileName.length - 3).toLowerCase();
  };
}

export default FileImport;
