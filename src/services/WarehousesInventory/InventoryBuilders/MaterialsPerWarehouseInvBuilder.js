class MaterialsPerWarehouseInvBuilder {
  constructor(parser, aggregator, sorter) {
    this.parser = parser;
    this.aggregator = aggregator;
    this.sorter = sorter;
  }

  build(dataString) {
    const dataItems = this.parser.parse(dataString);
    const output = this.aggregator.aggregate(dataItems);
    this.sorter.sort(output);
    return output;
  }

  getErrors = () => {
    return this.parser.invalidLines;
  };
}

export default MaterialsPerWarehouseInvBuilder;
