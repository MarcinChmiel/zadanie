class MaterialsInventoryDocumentParser {
  lineSplitChar = ";";
  stocksSplitChar = "|";

  invalidLines = [];

  parse = (dataString) => {
    if (!dataString || dataString.trim().length === 0) {
      throw new Error("file is empty");
    }
    let lines = this.splitToLines(dataString);
    lines = this.removeCommentLines(lines);
    lines = this.filterOutInvalidLines(lines);
    const materials = this.parseLines(lines);
    return materials;
  };

  splitToLines = (dataString) => {
    return dataString.match(/[^\r\n]+/g);
  };

  removeCommentLines = (lines) => {
    return lines.filter((l) => l.charAt(0) !== "#");
  };

  filterOutInvalidLines = (lines) => {
    const validLines = [];

    for (const line of lines) {
      if (line.trim().length === 0) {
        continue;
      }

      if (line.split(this.lineSplitChar).length !== 3) {
        this.invalidLines.push(line);
        continue;
      }
      validLines.push(line);
    }
    return validLines;
  };

  parseLines = (lines) => {
    const materials = [];

    for (const line of lines) {
      const lineSplit = line.split(this.lineSplitChar);
      if (this.isAnySplitSectionInvalid(lineSplit)) {
        this.invalidLines.push(line);
        continue;
      }
      const materialName = lineSplit[0].trim();
      const materialId = lineSplit[1].trim();

      const stockStates = this.parseStocks(lineSplit[2].trim());

      stockStates.map((stockState) =>
        materials.push(
          new MaterialStockItem(
            materialName,
            materialId,
            stockState.stockId,
            stockState.items
          )
        )
      );
    }
    return materials;
  };

  isAnySplitSectionInvalid = (lineSplit) => {
    return (
      lineSplit.filter((linePart) => linePart.trim().length === 0).length > 0
    );
  };

  parseStocks = (stocksStatesString) => {
    const statesArray = stocksStatesString.split(this.stocksSplitChar);
    const stockStates = statesArray.map((ss) => this.mapToStockState(ss));
    return stockStates;
  };

  mapToStockState(stateString) {
    const state = stateString.split(",");
    return new StockState(state[0].trim(), state[1].trim());
  }
}

export default MaterialsInventoryDocumentParser;

class MaterialStockItem {
  constructor(materialName, materialId, stockId, items) {
    this.materialName = materialName;
    this.materialId = materialId;
    this.stockId = stockId;
    this.items = items;
  }
}

class StockState {
  constructor(stockId, items) {
    this.stockId = stockId;
    this.items = items;
  }
}
