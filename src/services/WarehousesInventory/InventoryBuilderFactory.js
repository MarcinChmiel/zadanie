import MaterialsPerWarehouseInvBuilder from "./InventoryBuilders/MaterialsPerWarehouseInvBuilder";
import MaterialsInventoryDocumentParser from "./FileParsers/MaterialsInventoryDocumentParser";
import MaterialsPerWarehousesAggregator from "./Aggregators/MaterialsPerWarehousesAggregator";
import WarehousesDataSorter from "./Formaters/WarehousesDataSorter";

export class InventoryBuilderFactory {
  static getBuilder = (builderType) => {
    switch (builderType) {
      case WarehouseInventoryBuilderType.MaterialPerWarehouse:
        return this.getMaterialPerWarehouseBuilder();
      default:
        throw new Error("unknown warehouse inventory builder type");
    }
  };

  static getMaterialPerWarehouseBuilder = () => {
    return new MaterialsPerWarehouseInvBuilder(
      new MaterialsInventoryDocumentParser(),
      new MaterialsPerWarehousesAggregator(),
      new WarehousesDataSorter()
    );
  };
}

export class WarehouseInventoryBuilderType {
  static MaterialPerWarehouse = "materialsPerWarehouse";
}
