class MaterialsPerWarehousesAggregator {
  aggregate = (materialStockItems) => {
    if (!materialStockItems || materialStockItems.length === 0) {
      return [];
    }

    let stockIds = this.distincListOfWarehouseIds(materialStockItems);
    const warehouses = this.buildWarehousesList(stockIds, materialStockItems);
    return warehouses;
  };

  buildWarehousesList = (stockIds, allStockItems) => {
    const warehouses = [];
    for (const stockId of stockIds) {
      const relevantStockItems = this.selectRelevantItems(
        stockId,
        allStockItems
      );
      warehouses.push(this.createWarehouse(stockId, relevantStockItems));
    }
    return warehouses;
  };

  createWarehouse = (stockId, relevantItems) => {
    const totalItems = this.countTotalItems(relevantItems);
    const warehouseMaterials = relevantItems.map(
      (i) => new WarehouseMaterial(i.materialId, i.items)
    );
    return new Warehouse(stockId, totalItems, warehouseMaterials);
  };

  distincListOfWarehouseIds = (materialStockItems) => {
    let stockIds = materialStockItems.map((item) => item.stockId);
    return [...new Set(stockIds)];
  };

  selectRelevantItems = (stockId, allStockItems) => {
    return allStockItems.filter((m) => m.stockId === stockId);
  };

  countTotalItems = (relevantItems) => {
    return relevantItems
      .map((m) => m.items)
      .reduce((total, nextCount) => parseInt(total) + parseInt(nextCount));
  };
}

export default MaterialsPerWarehousesAggregator;

class Warehouse {
  constructor(id, totalItems, materials) {
    this.id = id;
    this.totalItems = totalItems;
    this.materials = materials;
  }
}

class WarehouseMaterial {
  constructor(materialId, count) {
    this.materialId = materialId;
    this.count = count;
  }
}
