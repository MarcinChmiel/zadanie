class WarehousesDataSorter {
  sort = (warehouses) => {
    if (!warehouses || warehouses.length === 0) {
      return [];
    }
    this.orderWarehousesMaterials(warehouses);
    this.orderWarehouses(warehouses);
  };

  orderWarehousesMaterials = (warehouses) => {
    for (const warehouse of warehouses) {
      this.orderMaterials(warehouse);
    }
  };

  orderWarehouses = (warehouses) => {
    warehouses.sort(this.warehousesCompare);
  };

  orderMaterials = (warehouse) => {
    warehouse.materials.sort(this.materialsCompare);
  };

  warehousesCompare = (a, b) => {
    const itemsCountA = parseInt(a.totalItems);
    const itemsCountB = parseInt(b.totalItems);

    if (itemsCountA > itemsCountB) {
      return -1;
    }

    if (itemsCountA < itemsCountB) {
      return 1;
    }

    if (a.id < b.id) {
      return 1;
    }

    if (a.id > b.id) {
      return -1;
    }

    return 0;
  };

  materialsCompare = (a, b) => {
    return a.materialId.toLowerCase().localeCompare(b.materialId.toLowerCase());
  };
}

export default WarehousesDataSorter;
