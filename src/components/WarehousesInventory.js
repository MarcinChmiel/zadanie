import React from "react";
import Warehouse from "./Warehouse";

const WarehousesInventory = (props) => {
  const warehouses =
    props.warehouses && props.warehouses.length > 0 ? (
      props.warehouses.map((w) => (
        <Warehouse warehouse={w} key={w.id}></Warehouse>
      ))
    ) : (
      <p>no data imported</p>
    );

  return (
    <div class="row">
      <div class="col">
        <h3>Warehouses Inventory</h3>
        {warehouses}
      </div>
    </div>
  );
};

export default WarehousesInventory;
