import React from "react";

const ImportErrors = (props) => {
  if (props.noImport) {
    return null;
  }

  const style = {
    fontSize: "12px",
  };

  const errors = props.errors;

  const noErrors = (
    <div class="container">
      <div class="row">
        <div class="col">
          <h3>Import Errors</h3>
          <p>no errors</p>
        </div>
      </div>
    </div>
  );

  if (!errors || errors.length === 0) {
    return noErrors;
  }

  return (
    <div class="container">
      <div class="row">
        <div class="col">
          <h3>Import Errors</h3>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <p>Following lines were not imported due to invalid format:</p>
          <ul class="list-group">
            {errors.map((err) => (
              <li class="list-group-item" style={style}>
                key={err}>{err}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ImportErrors;
