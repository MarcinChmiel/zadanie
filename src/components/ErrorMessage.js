import React from "react";

const ErrorMessage = (props) => {
  const style = {
    fontSize: "12px",
  };

  if (props.message && props.message.trim().length > 0) {
    return (
      <div class="alert alert-danger" role="alert" style={style}>
        {props.message}
      </div>
    );
  } else {
    return null;
  }
};

export default ErrorMessage;
