import React from "react";

const Warehouse = (props) => {
  const warehouse = (
    <p>
      {props.warehouse.id} {`(total  ${props.warehouse.totalItems})`}
    </p>
  );

  const materials = props.warehouse.materials.map((m) => (
    <p key={m.materialId}>{`${m.materialId}: ${m.count}`}</p>
  ));

  return (
    <div>
      <div class="card">
        <div class="card-header">
          <p>{warehouse}</p>
        </div>
        <div class="card-body">
          <p class="card-text">{materials}</p>
        </div>
      </div>
      <br />
    </div>
  );
};

export default Warehouse;
