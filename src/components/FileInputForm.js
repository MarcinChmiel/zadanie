import React from "react";

const FileInputForm = (props) => {
  return (
    <div class="row">
      <div class="col">
        <h3>Import file</h3>
        <div class="input-group mb-3">
          <div class="custom-file">
            <input
              type="file"
              class="custom-file-input"
              id="inputGroupFile01"
              aria-describedby="inputGroupFileAddon01"
              onChange={props.onFileSelected}
            />
            <label class="custom-file-label" for="inputGroupFile01">
              Choose file
            </label>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FileInputForm;
