import React, { Component } from "react";
import "./App.css";
import FileInputForm from "./components/FileInputForm";
import FileImport from "./services/FileImport";
import WarehousesInventory from "./components/WarehousesInventory";
import {
  InventoryBuilderFactory,
  WarehouseInventoryBuilderType,
} from "./services/WarehousesInventory/InventoryBuilderFactory";
import ImportErrors from "./components/ImportErrors";
import "bootstrap/dist/css/bootstrap.min.css";
import ErrorMessage from "./components/ErrorMessage";

class App extends Component {
  state = {
    warehouses: [],
    invalidLines: [],
    noImport: true,
    errorMessage: "",
  };

  fileSelectedHandler = async (event) => {
    try {
      if (!event.target.files[0]) {
        return;
      }
      const fileString = await FileImport.readTextFileToStringAsync(
        event.target.files[0]
      );
      const inventoryDataBuilder = InventoryBuilderFactory.getBuilder(
        WarehouseInventoryBuilderType.MaterialPerWarehouse
      );
      const warehouses = inventoryDataBuilder.build(fileString);
      const invalidLines = inventoryDataBuilder.getErrors();

      this.setState({
        warehouses: warehouses,
        invalidLines: invalidLines,
        noImport: false,
        errorMessage: "",
      });
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
  };

  render() {
    return (
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <FileInputForm
              onFileSelected={this.fileSelectedHandler}
            ></FileInputForm>
            <ErrorMessage message={this.state.errorMessage}></ErrorMessage>
          </div>
          <div class="col-md-5">
            <WarehousesInventory
              warehouses={this.state.warehouses}
            ></WarehousesInventory>
          </div>
          <div class="col-md-5">
            <ImportErrors
              errors={this.state.invalidLines}
              noImport={this.state.noImport}
            ></ImportErrors>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
